##Figure 4  R script
rm(list = ls())
library(ggplot2)
library(ggpubr)
library(data.table)
library(gridExtra)
library(grid)
library(reshape)
library(ggrepel)
library(limma)
library(DESeq2)
library(TissueEnrich)
library(tximport)
library(SummarizedExperiment)
library(qvalue)
library(mice)
library(car)
library(lme4)

tx2genes <- function(){
	mart <- biomaRt::useMart(biomart = "ensembl", dataset = "hsapiens_gene_ensembl")
	t2g <- biomaRt::getBM(attributes = c("uniprot_gn_id","external_gene_name"), mart = mart)
	t2g <- dplyr::rename(t2g, protein_name=uniprot_gn_id, GENEID = external_gene_name)
	return(t2g)
	}
t2g <- tx2genes()

#Figure 4A - Plot clinical values

pairs <- stack(list("S01RS6"="S022QS","S01Y9G"="S022UK","S01WCI"="S0232Z","S01TEQ"="S0234V","S01WXD"="S023EB","S01WFC"="S023F9","S01Y7K"="S023H5","S022TM"="S023PQ","S01XJ0"="S023RM","S01SYR"="S0240Z","S022GB"="S0245P"))
set.seed(100) 
pathToGitFolder=""
source(paste0(pathToGitFolder,"/scripts/2_Data_Post_processing/functions.R"))
d=fread(paste0(pathToGitFolder,"/Tables/anthropometric_values.csv"))
d=subset(d,select=c(COHORT,GENDER,AGE,LAR,GLC,TC,TG,HDL,LDL,ALT,hsCRP,INSULIN,HOMA_IR,AT_IR,AST)) 
d=data.frame(d[,1:2],sapply(d[,3:ncol(d)], function(x) as.numeric(as.character(x))))  
d=d[grep("WP10_donor|Obese|Lipodystrophy|Post_surgery",d$COHORT),]
d=d[rowSums(is.na(d)) <=4, ]
tx2gene <- as.data.frame(fread(paste0(pathToGitFolder,"/Tables/Ensembl_hg38_v86_geneName_txName.csv")))
colnames(tx2gene)=c('TXNAME','GENEID')

#Impute missing data using mice package
d2 <- mice(d[,3:ncol(d)])
d2 <- complete(d2)
d=cbind(d[,1:2],d2)
d_bar=d[which(d$COHORT=="Obese"),]
d_lipo=d[which(d$COHORT=="Lipodystrophy"),]
d_don=d[which(d$COHORT=="WP10_donor"),]
d_post=d[which(d$COHORT=="Post_surgery"),]

rownames(d)=d$SAMPLE_NAME
d$SAMPLE_NAME=NULL
d$GENDER=as.factor(d$GENDER)
d$AGE=as.factor(d$AGE)
d2=rbind(d_don,d_bar,d_lipo,d_post)

d2.bar=rbind(d_don,d_bar)
d2.lipo=rbind(d_don,d_lipo)
d2.lipo_post=rbind(d_lipo,d_post)
d2.lipo_bar=rbind(d_lipo,d_bar)

varToShow=c("COHORT","LAR","GLC","TC","TG","HDL","LDL","ALT","hsCRP","INSULIN","HOMA_IR","AT_IR","AST")
d2=d2[,colnames(d2) %in% varToShow]
d2$COHORT <- factor(d2$COHORT , levels = c("WP10_donor","Lipodystrophy", "Obese", "Post_surgery"))
d2=d2[order(d2$COHORT), ]
m2=melt(d2)
colnames(m2)=c("Cohort","Variable","Value")
m2$Cohort=factor(m2$Cohort , levels = c("WP10_donor","Lipodystrophy", "Obese", "Post_surgery"))

covariates=c("GENDER","AGE") #,"WEIGHT")

#covariates=c("GENDER") #,"WEIGHT")
covar=paste0("COHORT+",paste(covariates, collapse="+"))

var=varToShow[-c(which(c("COHORT") %in% varToShow))]

anovaTypeII.bar=list()
anovaTypeII.lipo=list()
anovaTypeII.post=list()
anovaTypeII.lipobar=list()

for (i in 1:length(var)){
  form <- as.formula(paste(var[i], "~",covar))
  lmMod <- lm(formula=form, data=d2.bar)
  a2=Anova(lmMod, type="III")
  anovaTypeII.bar[[i]]=c(var[i],a2[,"Pr(>F)"][[2]])
}

for (i in 1:length(var)){
  form <- as.formula(paste(var[i], "~",covar))
  lmMod <- lm(formula=form, data=d2.lipo)
  a2=Anova(lmMod, type="III")
  anovaTypeII.lipo[[i]]=c(var[i],a2[,"Pr(>F)"][[2]])
}

for (i in 1:length(var)){
  form <- as.formula(paste(var[i], "~",covar))
  lmMod <- lm(formula=form, data=d2.lipo_post)
  a2=Anova(lmMod, type="III")
  anovaTypeII.post[[i]]=c(var[i],a2[,"Pr(>F)"][[2]])
}

for (i in 1:length(var)){
  form <- as.formula(paste(var[i], "~",covar))
  lmMod <- lm(formula=form, data=d2.lipo_bar)
  a2=Anova(lmMod, type="III")
  anovaTypeII.lipobar[[i]]=c(var[i],a2[,"Pr(>F)"][[2]])
}

#Obese vs lean
anovaTypeII.bar = do.call(rbind, anovaTypeII.bar)
anovaTypeII.bar=as.data.frame(anovaTypeII.bar)
colnames(anovaTypeII.bar)=c("Variable","pAdj")
rownames(anovaTypeII.bar)=anovaTypeII.bar$Variable
anovaTypeII.bar$pAdj=as.numeric.factor(anovaTypeII.bar$pAdj)
anovaTypeII.bar$signif[anovaTypeII.bar$pAdj>0.05]="ns"
anovaTypeII.bar$signif[anovaTypeII.bar$pAdj>0.01 & anovaTypeII.bar$pAdj<=0.05]="*"
anovaTypeII.bar$signif[anovaTypeII.bar$pAdj>0.001 & anovaTypeII.bar$pAdj<=0.01]="**"
anovaTypeII.bar$signif[anovaTypeII.bar$pAdj>0.0001 & anovaTypeII.bar$pAdj<=0.001]="***"
anovaTypeII.bar$signif[anovaTypeII.bar$pAdj<=0.0001]="****"
anovaTypeII.bar

#         Variable         pAdj signif
# LAR          LAR 1.030142e-33   ****
# GLC          GLC 7.628959e-01     ns
# TC            TC 1.197868e-01     ns
# TG            TG 8.291083e-02     ns
# HDL          HDL 2.167716e-02      *
# LDL          LDL 1.655786e-01     ns
# ALT          ALT 4.230073e-01     ns
# hsCRP      hsCRP 1.341735e-10   ****
# INSULIN  INSULIN 1.271505e-01     ns
# HOMA_IR  HOMA_IR 1.215618e-01     ns
# AT_IR      AT_IR 3.370837e-05   ****
# AST          AST 5.906734e-01     ns

#Lipo vs lean
anovaTypeII.lipo = do.call(rbind, anovaTypeII.lipo)
anovaTypeII.lipo=as.data.frame(anovaTypeII.lipo)
colnames(anovaTypeII.lipo)=c("Variable","pAdj")
rownames(anovaTypeII.lipo)=anovaTypeII.lipo$Variable
anovaTypeII.lipo$pAdj=as.numeric.factor(anovaTypeII.lipo$pAdj)
anovaTypeII.lipo$signif[anovaTypeII.lipo$pAdj>0.05]="ns"
anovaTypeII.lipo$signif[anovaTypeII.lipo$pAdj>0.01 & anovaTypeII.lipo$pAdj<=0.05]="*"
anovaTypeII.lipo$signif[anovaTypeII.lipo$pAdj>0.001 & anovaTypeII.lipo$pAdj<=0.01]="**"
anovaTypeII.lipo$signif[anovaTypeII.lipo$pAdj>0.0001 & anovaTypeII.lipo$pAdj<=0.001]="***"
anovaTypeII.lipo$signif[anovaTypeII.lipo$pAdj<=0.0001]="****"
anovaTypeII.lipo
#         Variable         pAdj signif
# LAR          LAR 9.945706e-01     ns
# GLC          GLC 6.250322e-06   ****
# TC            TC 1.498079e-02      *
# TG            TG 1.189462e-13   ****
# HDL          HDL 1.694952e-06   ****
# LDL          LDL 2.275208e-06   ****
# ALT          ALT 1.827865e-08   ****
# hsCRP      hsCRP 5.489647e-01     ns
# INSULIN  INSULIN 1.747596e-03     **
# HOMA_IR  HOMA_IR 2.474394e-06   ****
# AT_IR      AT_IR 7.221596e-10   ****
# AST          AST 4.308807e-05   ****

#Post vs Lean
anovaTypeII.post = do.call(rbind, anovaTypeII.post)
anovaTypeII.post=as.data.frame(anovaTypeII.post)
colnames(anovaTypeII.post)=c("Variable","pAdj")
rownames(anovaTypeII.post)=anovaTypeII.post$Variable
anovaTypeII.post$pAdj=as.numeric.factor(anovaTypeII.post$pAdj)
anovaTypeII.post$signif[anovaTypeII.post$pAdj>0.05]="ns"
anovaTypeII.post$signif[anovaTypeII.post$pAdj>0.01 & anovaTypeII.post$pAdj<=0.05]="*"
anovaTypeII.post$signif[anovaTypeII.post$pAdj>0.001 & anovaTypeII.post$pAdj<=0.01]="**"
anovaTypeII.post$signif[anovaTypeII.post$pAdj>0.0001 & anovaTypeII.post$pAdj<=0.001]="***"
anovaTypeII.post$signif[anovaTypeII.post$pAdj<=0.0001]="****"
anovaTypeII.post

#         Variable        pAdj signif
# LAR          LAR 0.055642480     ns
# GLC          GLC 0.041268468      *
# TC            TC 0.607552707     ns
# TG            TG 0.026446152      *
# HDL          HDL 0.030681535      *
# LDL          LDL 0.010790118      *
# ALT          ALT 0.022406666      *
# hsCRP      hsCRP 0.735014464     ns
# INSULIN  INSULIN 0.635546347     ns
# HOMA_IR  HOMA_IR 0.641223738     ns
# AT_IR      AT_IR 0.264445915     ns
# AST          AST 0.008584747     **

anovaTypeII.lipobar = do.call(rbind, anovaTypeII.lipobar)
anovaTypeII.lipobar=as.data.frame(anovaTypeII.lipobar)
colnames(anovaTypeII.lipobar)=c("Variable","pAdj")
rownames(anovaTypeII.lipobar)=anovaTypeII.lipobar$Variable
anovaTypeII.lipobar$pAdj=as.numeric.factor(anovaTypeII.lipobar$pAdj)
anovaTypeII.lipobar$signif[anovaTypeII.lipobar$pAdj>0.05]="ns"
anovaTypeII.lipobar$signif[anovaTypeII.lipobar$pAdj>0.01 & anovaTypeII.lipobar$pAdj<=0.05]="*"
anovaTypeII.lipobar$signif[anovaTypeII.lipobar$pAdj>0.001 & anovaTypeII.lipobar$pAdj<=0.01]="**"
anovaTypeII.lipobar$signif[anovaTypeII.lipobar$pAdj>0.0001 & anovaTypeII.lipobar$pAdj<=0.001]="***"
anovaTypeII.lipobar$signif[anovaTypeII.lipobar$pAdj<=0.0001]="****"
anovaTypeII.lipobar

#Lipo vs Obese
#         Variable         pAdj signif
# LAR          LAR 2.909039e-06   ****
# GLC          GLC 1.973536e-02      *
# TC            TC 4.864893e-01     ns
# TG            TG 6.490480e-02     ns
# HDL          HDL 4.570997e-02      *
# LDL          LDL 4.987119e-03     **
# ALT          ALT 6.910334e-04    ***
# hsCRP      hsCRP 3.371608e-02      *
# INSULIN  INSULIN 4.314090e-01     ns
# HOMA_IR  HOMA_IR 1.698346e-01     ns
# AT_IR      AT_IR 6.075518e-01     ns
# AST          AST 9.912940e-03     **

cols = c("WP10_donor"="#F3766E","Lipodystrophy"="#0D56A7","Obese"="#199E77","Post_surgery"="#99CA3C")
m2$Cohort=as.character.factor(m2$Cohort)
#m2=m2[which(m2$Variable!="AGE"),]  ##Remove AGE for plotting
p2=ggplot(m2, aes(x=factor(Variable), y=log(Value+1))) + geom_boxplot(alpha = 0.5,width = 0.9, outlier.colour = NA,show.legend = NA,aes(fill=Cohort,colour=Cohort),position=position_dodge(width=1)) + geom_point(position=position_jitterdodge(dodge.width=1,jitter.width=0.2),aes(fill=Cohort,colour=Cohort),size = 1) + theme(text = element_text(size=12, color="black"), axis.text = element_text(size=10, color="black"),axis.title=element_text(size=10,face="bold"),axis.title.x = element_text(margin = margin(t = 40, r = 0, b = 0, l = 0)),axis.title.y = element_text(margin = margin(t = 0, r = 40, b = 0, l = 0)),legend.key.size = unit(4,"line"),panel.background = element_rect(fill="white"),panel.grid.minor.y = element_line(size=3),panel.grid.major = element_line(colour = "lightgrey"),plot.title = element_text(,face="bold",margin=margin(t = 0, r = 0, b = 40, l = 0),hjust = 0.5)) + labs(x="Laboratory values", y="log(value)")
p2=p2+scale_fill_manual(values = cols)+scale_colour_manual(values = cols)
ggsave(paste0(pathToGitFolder,"/Figures/donor_Bariatric-patients_value_distribution_2.pdf"), p2,w=20,h=15,useDingbats=FALSE) 

#obese/post comp conditional multiple logistic regression
d=fread(paste0(pathToGitFolder,"/Tables/anthropometric_values.csv"))
d=subset(d,select=c(ID,COHORT,GENDER,AGE,LAR,GLC,TC,TG,HDL,LDL,ALT,hsCRP,INSULIN,HOMA_IR,AT_IR,AST)) 
d=data.frame(d[,1:3],sapply(d[,4:ncol(d)], function(x) as.numeric(as.character(x))))  
d=d[grep("Obese|Post_surgery",d$COHORT),]
d$ID2=d$ID
d$ID2[which(d$COHORT=="Post_surgery")]=as.character(pairs$ind[match(d$ID2[which(d$COHORT=="Post_surgery")], pairs$values)])
d=d[which(d$ID2 %in% names(table(d$ID2)[table(d$ID2)>1])),]
d$ID=d$ID2
d$ID2=NULL
d2 <- mice(d[,4:ncol(d)])
d2 <- complete(d2)
d=cbind(d[,1:3],d2)
d=d[order(d$ID),]
d$pair=sort(rep(seq(1, 9, by = 1),2))
d=d[order(d$COHORT),]
d$COHORT_2=c(rep(0,9),rep(1,9))

d=data.frame(d[,1:4],sapply(d[,5:16], function(x) log(x+1)),d[,17:18]) 

var=c("LAR","GLC","TC","TG","HDL","LDL","ALT","hsCRP","INSULIN","HOMA_IR","AT_IR","AST")
covariates=c("GENDER","AGE") #,"WEIGHT")
covar=paste0("COHORT_2+",paste(covariates, collapse="+"))

glmer.post=list()

for (i in 1:length(var)){
  form <- as.formula(paste(var[i], "~",covar,"+(1|pair)"))
  glmerMod <- summary(glmer(form,family=Gamma(link="log"),data=d,glmerControl(optimizer = "nloptwrap", optCtrl = list(maxfun = 1000000))))
  glmer.post[[i]]=c(var[i],glmerMod$coefficients[,4][[2]])
}

glmer.post = do.call(rbind, glmer.post)
glmer.post=as.data.frame(glmer.post)
colnames(glmer.post)=c("Variable","pAdj")
rownames(glmer.post)=glmer.post$Variable
glmer.post$pAdj=as.numeric.factor(glmer.post$pAdj)
glmer.post$signif[glmer.post$pAdj>0.05]="ns"
glmer.post$signif[glmer.post$pAdj>0.01 & glmer.post$pAdj<=0.05]="*"
glmer.post$signif[glmer.post$pAdj>0.001 & glmer.post$pAdj<=0.01]="**"
glmer.post$signif[glmer.post$pAdj>0.0001 & glmer.post$pAdj<=0.001]="***"
glmer.post$signif[glmer.post$pAdj<=0.0001]="****"
glmer.post

#Obese post paired
#         Variable         pAdj signif
# LAR          LAR 4.921394e-07   ****
# GLC          GLC 8.326716e-01     ns
# TC            TC 1.696602e-01     ns
# TG            TG 2.635463e-09   ****
# HDL          HDL 1.837452e-03     **
# LDL          LDL 8.135216e-01     ns
# ALT          ALT 2.524616e-01     ns
# hsCRP      hsCRP 7.707177e-05   ****
# INSULIN  INSULIN 1.945253e-01     ns
# HOMA_IR  HOMA_IR 1.990445e-01     ns
# AT_IR      AT_IR 7.093224e-03     **
# AST          AST 6.214296e-03     **

##Figure 4B
data=fread(paste0(pathToGitFolder,"/Tables/diffAnalysisSummary.csv"))
##Obese vs Post-surgery
data2=data[grep("Obese vs Post-surgery",data$Comparison),]
p = ggplot(melt(data2),aes(x=Experiment, y=as.numeric(value),fill=variable)) + geom_bar(stat="identity",position=position_dodge(),color="black") + scale_fill_manual(values=c("Platelets"="#FCE61F","Neutrophils"="#35B779","Monocytes"="#31688E","Macrophages"="#431853")) + theme_minimal()+ theme(axis.text.x = element_text(size=25),axis.text.y = element_text(size=25)) + geom_text(aes(label=value), vjust=-0.6, color="black",position = position_dodge(0.9), size=13) + labs(x="", y = "Number of features statistically different")
ggsave(p,file=paste0(pathToGitFolder,"/Figures/Figure4B.pdf"),wi=20,he=15)

##Figure 4C and D
###GTex parsing

gtex=fread(paste0(pathToGitFolder,"/Tables/GTEx_Analysis_2016-01-15_v7_RNASeQCv1.1.8_gene_median_tpm_subset.csv"))
gtex$gene_id=NULL
gtex.a <-aggregate(gtex, by=list(gtex$Description),FUN=mean, na.rm=TRUE)
rownames(gtex.a)=gtex.a$Group.1
gtex=subset(gtex.a,select=-c(Description,Group.1))
dim(gtex)
#53537 14

se<-SummarizedExperiment(assays = SimpleList(as.matrix(gtex)),rowData = row.names(gtex),colData = colnames(gtex))
output<-teGeneRetrieval(se)
gtex_tissue=data.frame(assay(output))
table(gtex_tissue$Tissue)
#                          All                   Artery_Aorta 
#                         48354                            417 
#               Artery_Coronary                  Artery_Tibial 
#                           302                            400 
#        Heart_Atrial_Appendage           Heart_Left_Ventricle 
#                           349                            286 
#                 Kidney_Cortex                          Liver 
#                           543                            739 
#                          Lung                       Pancreas 
#                           772                            380 
#Small_Intestine_Terminal_Ileum                         Spleen 
#                           868                           1286 
#                       Stomach                        Thyroid 
#                           345                            826 
#                   Whole_Blood 
#                           693


whole_blood_genes=gtex_tissue[which(gtex_tissue$Tissue=="Whole_Blood"),]
dim(whole_blood_genes)
# 693 3
length(whole_blood_genes$Group[which(whole_blood_genes$Group == "Group-Enriched")])
# 348
length(whole_blood_genes$Group[which(whole_blood_genes$Group == "Tissue-Enhanced")])
# 250
length(whole_blood_genes$Group[which(whole_blood_genes$Group == "Tissue-Enriched")])
# 95


##############################

###Protein abundance diff analysis Pre_post
pairs.st <- stack(list("S01RS6"="S022QS","S01Y9G"="S022UK","S01WCI"="S0232Z","S01TEQ"="S0234V","S01WXD"="S023EB","S01WFC"="S023F9","S01Y7K"="S023H5","S022TM"="S023PQ","S01XJ0"="S023RM","S01SYR"="S0240Z","S022GB"="S0245P"))
pairs = list("S01RS6"="S022QS","S01Y9G"="S022UK","S01WCI"="S0232Z","S01TEQ"="S0234V","S01WXD"="S023EB","S01WFC"="S023F9","S01Y7K"="S023H5","S022TM"="S023PQ","S01XJ0"="S023RM","S01SYR"="S0240Z","S022GB"="S0245P")

abundance.pre = read.csv(paste0(pathToGitFolder,"/Tables/proteomics_all_pre_2.csv"))
abundance.post = read.csv(paste0(pathToGitFolder,"/Tables/proteomics_all_post_2.csv"))

abundance.pre = abundance.pre[!is.na(abundance.pre[,1]),]
abundance.post = abundance.post[!is.na(abundance.post[,1]),]
rownames(abundance.pre)=abundance.pre$Accession
rownames(abundance.post)=abundance.post$Accession

prots=intersect(abundance.pre$Accession,abundance.post$Accession)

abundance.pre=abundance.pre[prots,]
abundance.post=abundance.post[prots,]

pre.ind=colnames(abundance.pre)
post.ind=colnames(abundance.post)
post.ind=intersect(post.ind,paste(unlist(pairs[pre.ind])))
pre.ind=f(post.ind)

abundance.pre=abundance.pre[,pre.ind]
abundance.post=abundance.post[,post.ind]

pre=as.matrix(as.data.frame(lapply(abundance.pre, as.numeric)))
rownames(pre)=rownames(abundance.pre)
post=as.matrix(as.data.frame(lapply(abundance.post, as.numeric)))
rownames(post)=rownames(abundance.post)

prepost=cbind(pre,post)
design <- model.matrix(~factor(c(rep(2,10),rep(1,10))))
colnames(design) <- c("Intercept", "Diff")
res.eb <- eb.fit(prepost, design)
res.eb.fdr=res.eb[which(res.eb$q.ord < 0.001),]
res.eb$protein_name=rownames(res.eb)
diffProts=rownames(res.eb.fdr)

##############################

###Load RNAseq data

samples <- as.data.frame(fread(paste0(pathToGitFolder,"/Tables/Manifest_RNAseq.csv")))
samples=samples[which(samples$COHORT %in% c("Obese","Post_surgery")),]
rownames(samples)=samples$SAMPLE_ID
files.samples <- file.path(samples$PATH, samples$SAMPLE_ID, "abundance.h5")
names(files.samples) <- paste0(samples$SAMPLE_ID, 1:length(files.samples))
names(files.samples)[1:9]=gsub('.{1}$', '', names(files.samples)[1:9]) ##remove last digit from colnames 1 to 9
names(files.samples)[10:length(files.samples)]=gsub('.{2}$', '', names(files.samples)[10:length(files.samples)]) ##remove 2 lasts digits from colnames 10 to end

all(file.exists(files.samples))
txi.genes <- tximport(files.samples, type = "kallisto", tx2gene = tx2gene) ## summarize abundance to gene-level

genes.tpm=txi.genes$abundance
genes.tpm.log=data.frame(log2(genes.tpm+1))
colnames(genes.tpm.log)=gsub("SLX.","SLX-",colnames(genes.tpm.log))
genes.tpm.log$GENEID=rownames(genes.tpm.log)
genes.tpm.log=merge(genes.tpm.log,t2g,by="GENEID")
genes.tpm.log=genes.tpm.log[which(genes.tpm.log$protein_name %in% diffProts),]
rownames(genes.tpm.log)=genes.tpm.log$GENEID
genes.tpm.log$GENEID=NULL

c1=samples$CELL_TYPE[match(colnames(genes.tpm.log),samples$SAMPLE_ID)]
c1=paste0(c1,"_",samples$COHORT)
c1[which(c1=="NA_Post_surgery")]="protein_name"
colnames(genes.tpm.log)=c1

gene_by_tissues=gtex_tissue[which(gtex_tissue$Gene %in% rownames(genes.tpm.log)),]
blood_spe_genes=intersect(as.character(whole_blood_genes$Gene),rownames(genes.tpm.log))

table(gene_by_tissues$Tissue)
#
#			  			   All                   Artery_Aorta 
#                           511                             10 
#               Artery_Coronary                  Artery_Tibial 
#                             9                             10 
#        Heart_Atrial_Appendage           Heart_Left_Ventricle 
#                             9                              8 
#                 Kidney_Cortex                          Liver 
#                             7                             10 
#                          Lung                       Pancreas 
#                             9                              1 
#Small_Intestine_Terminal_Ileum                         Spleen 
#                            12                             10 
#                       Stomach                        Thyroid 
#                             7                             10 
#                   Whole_Blood 
#                            13 


meanExpr.pre=data.frame("Platelets_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Platelets_Obese")]),"Neutrophils_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Neutrophils_Obese")]),"Monocytes_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Monocytes_Obese")]),"Macrophages_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Macrophages_Obese")]))
meanExpr.post=data.frame("Platelets_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Platelets_Post_surgery")]),"Neutrophils_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Neutrophils_Post_surgery")]),"Monocytes_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Monocytes_Post_surgery")]),"Macrophages_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Macrophages_Post_surgery")]))
meanExpr.pre.perc=round(meanExpr.pre/rowSums(meanExpr.pre), 2)
meanExpr.pre.perc$Gene=rownames(meanExpr.pre.perc)
meanExpr.pre$Gene=rownames(meanExpr.pre)
meanExpr.pre.perc=meanExpr.pre.perc[order(rowSums(meanExpr.pre.perc[,1:4])),]
meanExpr.pre=meanExpr.pre[order(rowSums(meanExpr.pre[,1:4])),]
meanExpr.pre.perc$Gene=factor(meanExpr.pre.perc$Gene,levels=meanExpr.pre.perc$Gene)
meanExpr.pre$Gene=factor(meanExpr.pre$Gene,levels=meanExpr.pre$Gene)
meanExpr.pre.melt.perc=melt(meanExpr.pre.perc)
meanExpr.pre.melt=melt(meanExpr.pre)

meanExpr.post.perc=round(meanExpr.post/rowSums(meanExpr.post), 2)
meanExpr.post.perc$Gene=rownames(meanExpr.post.perc)
meanExpr.post$Gene=rownames(meanExpr.post)
meanExpr.post.perc=meanExpr.post.perc[rownames(meanExpr.pre.perc),]
meanExpr.post=meanExpr.post[rownames(meanExpr.pre),]
meanExpr.post.perc$Gene=factor(meanExpr.post.perc$Gene,levels=meanExpr.post.perc$Gene)
meanExpr.post$Gene=factor(meanExpr.post$Gene,levels=meanExpr.post$Gene)
meanExpr.post.melt.perc=melt(meanExpr.post.perc)
meanExpr.post.melt=melt(meanExpr.post)

##Load DEG
neu=as.data.frame(fread(paste0(pathToGitFolder,"/Tables/RNAseq_Neutrophils_COHORT_Post_surgery_vs_Obese.csv")))
mono=as.data.frame(fread(paste0(pathToGitFolder,"/Tables/RNAseq_Monocytes_COHORT_Post_surgery_vs_Obese.csv")))
plt=as.data.frame(fread(paste0(pathToGitFolder,"/Tables/RNAseq_Platelets_COHORT_Post_surgery_vs_Obese.csv")))
macro=as.data.frame(fread(paste0(pathToGitFolder,"/Tables/RNAseq_Macrophages_COHORT_Post_surgery_vs_Obese.csv")))

rownames(neu)=neu$V1
rownames(mono)=mono$V1
rownames(plt)=plt$V1
rownames(macro)=macro$V1

neu.s=neu[neu$padj<0.05,]
neu.s=neu.s[complete.cases(neu.s),]
mono.s=mono[mono$padj<0.05,]
mono.s=mono.s[complete.cases(mono.s),]
plt.s=plt[plt$padj<0.05,]
plt.s=plt.s[complete.cases(plt.s),]
macro.s=macro[macro$padj<0.05,]
macro.s=macro.s[complete.cases(macro.s),]

allDEG=unique(c(rownames(neu.s),rownames(plt.s),rownames(mono.s),rownames(macro.s)))
meanExpr.pre$DEG=allDEG[match(meanExpr.pre$Gene,allDEG)]
meanExpr.pre$DEG[!is.na(meanExpr.pre$DEG)]="*"
meanExpr.pre$DEG[is.na(meanExpr.pre$DEG)]=""
meanExpr.pre$Gene=paste0(meanExpr.pre$Gene,meanExpr.pre$DEG)
meanExpr.pre$DEG=NULL
meanExpr.pre$GENEID=rownames(meanExpr.pre)

for (i in 1:length(unique(gene_by_tissues$Tissue))){
print(as.character(unique(gene_by_tissues$Tissue)[i]))
print(length(intersect(allDEG,intersect(gene_by_tissues[which(gene_by_tissues$Tissue==unique(gene_by_tissues$Tissue)[i]),]$Gene,rownames(genes.tpm.log)))))
}
# "All"
# 201
# "Small_Intestine_Terminal_Ileum"
# 3
# "Whole_Blood"
# 6
# "Liver"
# 2
# "Kidney_Cortex"
# 1
# "Heart_Atrial_Appendage"
# 0
# "Artery_Tibial"
# 1
# "Artery_Aorta"
# 2
# "Artery_Coronary"
# 1
# "Thyroid"
# 2
# "Lung"
# 1
# "Stomach"
# 0
# "Heart_Left_Ventricle"
# 1
# "Spleen"
# 3
# "Pancreas"
# 0

##############################
##Figure 4C

genes.tpm.log=genes.tpm.log[blood_spe_genes,]
meanExpr.pre=data.frame("Platelets_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Platelets_Obese")]),"Neutrophils_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Neutrophils_Obese")]),"Monocytes_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Monocytes_Obese")]),"Macrophages_Obese"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Macrophages_Obese")]))
meanExpr.post=data.frame("Platelets_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Platelets_Post_surgery")]),"Neutrophils_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Neutrophils_Post_surgery")]),"Monocytes_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Monocytes_Post_surgery")]),"Macrophages_Post_surgery"=rowMeans(genes.tpm.log[,colnames(genes.tpm.log) %in% c("Macrophages_Post_surgery")]))
meanExpr.pre.perc=round(meanExpr.pre/rowSums(meanExpr.pre), 2)
meanExpr.pre.perc$Gene=rownames(meanExpr.pre.perc)
meanExpr.pre$Gene=rownames(meanExpr.pre)
meanExpr.pre.perc=meanExpr.pre.perc[order(rowSums(meanExpr.pre.perc[,1:4])),]
meanExpr.pre=meanExpr.pre[order(rowSums(meanExpr.pre[,1:4])),]
meanExpr.pre.perc$Gene=factor(meanExpr.pre.perc$Gene,levels=meanExpr.pre.perc$Gene)
meanExpr.pre$Gene=factor(meanExpr.pre$Gene,levels=meanExpr.pre$Gene)
meanExpr.pre.melt.perc=melt(meanExpr.pre.perc)
meanExpr.pre.melt=melt(meanExpr.pre)

meanExpr.post.perc=round(meanExpr.post/rowSums(meanExpr.post), 2)
meanExpr.post.perc$Gene=rownames(meanExpr.post.perc)
meanExpr.post$Gene=rownames(meanExpr.post)
meanExpr.post.perc=meanExpr.post.perc[rownames(meanExpr.pre.perc),]
meanExpr.post=meanExpr.post[rownames(meanExpr.pre),]
meanExpr.post.perc$Gene=factor(meanExpr.post.perc$Gene,levels=meanExpr.post.perc$Gene)
meanExpr.post$Gene=factor(meanExpr.post$Gene,levels=meanExpr.post$Gene)
meanExpr.post.melt.perc=melt(meanExpr.post.perc)
meanExpr.post.melt=melt(meanExpr.post)
meanExpr.pre$DEG=allDEG[match(meanExpr.pre$Gene,allDEG)]
meanExpr.pre$DEG[!is.na(meanExpr.pre$DEG)]="*"
meanExpr.pre$DEG[is.na(meanExpr.pre$DEG)]=""
meanExpr.pre$Gene=paste0(meanExpr.pre$Gene,meanExpr.pre$DEG)
meanExpr.pre$DEG=NULL
meanExpr.pre$GENEID=rownames(meanExpr.pre)


a=merge(res.eb,t2g,by='protein_name')
b=merge(a,meanExpr.pre,by="GENEID",all.x=T)
b$al=0.8
b$al[which(!is.na(b$Platelets_Obese))]=1
###Draw Volcanoplot
volcano=ggplot(b,aes(x=logFC,y=-log10(q.ord),label = GENEID)) + 
		ylim(0,26) + 
		xlim(-6,6) + 
		theme(legend.position="none",text = element_text(size=20)) +
		theme_minimal() +
		geom_point(color = "dark grey",aes(alpha = al)) + 
		geom_point(color = ifelse(b$logFC > 0 & b$q.ord < max(res.eb.fdr$q.ord), "red", ifelse(b$logFC <= 0 & b$q.ord < max(res.eb.fdr$q.ord), "blue","black")),aes(alpha = al)) + 
		geom_hline(yintercept = -log10(max(res.eb.fdr$q.ord)), linetype = 3) + 
		labs(x = "logFC", y = "-log10(FDR)", title="Obese Post Proteomics") +
		geom_text_repel(
    		data         = subset(b, !is.na(b$Platelets_Obese)),
			hjust		 = 3,
			vjust        = 0,
    		segment.size = 0.2,
			xlim		 = c(-2,-6)
  		)# +
		geom_text_repel(
    		data         = subset(res.eb, (logFC < -2 & q.ord < 1e-20)),
			hjust		 = 0,
			vjust        = 0,
    		segment.size = 0.2,
			xlim		 = c(-2,-10)
  		)
		
		
		
ggsave(volcano,file=paste0(pathToGitFolder,"/Figures/Figure4C.pdf"),he=10,useDingbats=FALSE)   

##############################
##Figure 4D plot vertical barplot gene expression of whole blood specific genes vs diff abundant proteins

m.pre=ggplot(meanExpr.pre.melt,aes(x = Gene, y = value,fill = variable)) + 
		geom_bar(stat = "identity") +
		ggtitle("Obese") + scale_fill_manual(values=c("Platelets_Obese"="#FCE61F","Neutrophils_Obese"="#35B779","Monocytes_Obese"="#31688E","Macrophages_Obese"="#431853")) + theme(legend.position="none",axis.title.x = element_blank(),panel.background=element_blank(),axis.title.y = element_blank(),axis.text.y = element_blank(),axis.ticks.y = element_blank(),plot.margin = unit(c(1,-1,1,0), "mm")) +
  		scale_y_reverse() + coord_flip()  
gg1=ggplot_gtable(ggplot_build(m.pre))

m.post=ggplot(meanExpr.post.melt,aes(x = Gene, y = value,fill = variable)) + 
		geom_bar(stat = "identity") +
		ggtitle("Post_Surgery") + scale_fill_manual(values=c("Platelets_Post_surgery"="#FCE61F","Neutrophils_Post_surgery"="#35B779","Monocytes_Post_surgery"="#31688E","Macrophages_Post_surgery"="#431853")) +  theme(legend.position="none",axis.title.x = element_blank(), panel.background=element_blank(),axis.title.y = element_blank(), axis.text.y = element_blank(), axis.ticks.y = element_blank(),plot.margin = unit(c(1,0,1,-1), "mm")) +
  		coord_flip()
gg2=ggplot_gtable(ggplot_build(m.post))

g.mid<-ggplot(meanExpr.pre,aes(x=1,y=Gene))+
  geom_text(aes(label=Gene),size=7.5)+
  ggtitle("")+
  ylab(NULL)+
  theme(axis.title=element_blank(),
        panel.grid=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        panel.background=element_blank(),
        axis.text.x=element_text(color=NA),
        axis.ticks.x=element_line(color=NA),
        plot.margin = unit(c(1,-0.5,1,-0.5), "mm"))
		
gg.mid <- ggplot_gtable(ggplot_build(g.mid))

ggsave(grid.arrange(gg1,gg.mid,gg2,ncol=3,widths=c(5/12,2/12,5/12)),file=paste0(pathToGitFolder,"/Figures/Figure4D.pdf"),wi=40,he=25)

##############################
##Figure 4E
#Adhesion tests
p=as.data.frame(fread(paste0(pathToGitFolder,"/Tables/platelets_adhesion_test.csv")))
colnames(p)=c("Sample_ID","Status",colnames(p[,3:ncol(p)]))
p=p[p$Sample_ID %in% names(which(table(p$Sample_ID)==2)),]
p$Status=gsub("pre","Obese",p$Status)
p$Status=gsub("post","Post_surgery",p$Status)
p$Status=factor(p$Status,levels=c("Obese","Post_surgery"))

for (i in 3:ncol(p)){
print(paste0(colnames(p)[i],":",t.test(p[1:9,i],p[10:18,i] , paired = TRUE, alternative = "two.sided")$p.value))
}

#"HEPES_RFU_Av:0.0482990819551696"
#"DTT_pc_Ad:0.0557730866673926"
#"LBP_pc_Ad:0.0122967745266231"
#"PAM3_pc_Ad:0.00262243017015689"
#"PAF_pc_Ad:0.00163871628227788"
#"fMLP_pc_Ad:0.00862206764806315"
#"TNF_pc_Ad:0.288097647087208"
#"PMA_pc_Ad:0.92965527322414"


for (i in 3:ncol(p)){
print(paste0(colnames(p)[i],":",wilcox.test(p[1:9,i],p[10:18,i] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"HEPES_RFU_Av:0.0546875"
#"DTT_pc_Ad:0.07421875"
#"LBP_pc_Ad:0.01171875"
#"PAM3_pc_Ad:0.0078125"
#"PAF_pc_Ad:0.00390625"
#"fMLP_pc_Ad:0.01953125"
#"TNF_pc_Ad:0.359375"
#"PMA_pc_Ad:0.65234375"

for (i in 3:ncol(p)){
pdf(paste0(pathToGitFolder,"/Figures/Figure4E_",colnames(p[i]),".pdf"))
g=ggpaired(p, x = "Status", y = colnames(p[i]),color = "Status", line.color = "gray", line.size = 0.6,palette = c("#199E77","#88C540")) + ggtitle(colnames(p[i])) + stat_compare_means(method = "wilcox.test",paired = TRUE)+ theme(text = element_text(color = "black",size=20)) #+geom_text(aes(label = Sample_ID), size=3)
print(g)
dev.off()
}


#Neutrophils tests
p=as.data.frame(fread(paste0(pathToGitFolder,"/Tables/neutrophils_activity_test.csv")))
p=p[p$Idpairs %in% names(which(table(p$Idpairs)==2)),]
colnames(p)=c("ID","IDpairs","Status",colnames(p[,4:ncol(p)]))
rownames(p)=p$ID
p$Status=factor(p$Status,levels=c("Pre","Post"))

p2=p[,colnames(p)[colSums(is.na(p)) > 0]]
p2=cbind(p$Status,p$ID,p2)
p2=p2[complete.cases(p2),]
colnames(p2)=gsub("p\\$","",colnames(p2))
p2=p2[which(rownames(p2) != "S01SYR"),]

p=p[,colnames(p)[colSums(is.na(p)) == 0]]
p$Status=gsub("Pre","Obese",p$Status)
p$Status=gsub("Post","Post_surgery",p$Status)
p$Status=factor(p$Status,levels=c("Obese","Post_surgery"))
p[,4:ncol(p)]=data.matrix(p[,4:ncol(p)])
p2[,4:ncol(p2)]=data.matrix(p2[,4:ncol(p2)])

for (i in 4:ncol(p)){
print(paste0(colnames(p)[i],":",t.test(p[1:8,i],p[9:16,i] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"DS1_UNS_FS:0.873278494299631"
#"DS1_UNS_SS:0.698376214236126"
#"CD14:0.452484598618177"

for (i in 3:ncol(p2)){
print(paste0(colnames(p2)[i],":",t.test(p2[1:7,i],p2[8:14,i] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"CD66b:0.365204581155216"
#"CD16:0.0113576949955985"
#"CD63:0.111684469823465"
#"CD62L:0.326742906636496"
#"CD11b:0.702047785086801"
#"CD32:0.00477170851984722"

for (i in 4:ncol(p)){
print(paste0(colnames(p)[i],":",wilcox.test(p[1:8,i],p[9:16,i] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"DS1_UNS_FS:1"
#"DS1_UNS_SS:0.640625"
#"CD14:0.4609375"

for (i in 3:ncol(p2)){
print(paste0(colnames(p2)[i],":",wilcox.test(p2[1:7,i],p2[8:14,i] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"CD66b:0.578125"
#"CD16:0.015625"
#"CD63:0.21875"
#"CD62L:0.375"
#"CD11b:0.8125"
#"CD32:0.015625"

for (i in 3:ncol(p)){
pdf(paste0(pathToGitFolder,"/Figures/Figure4E_",colnames(p[i]),".pdf"))
g=ggpaired(p, x = "Status", y = colnames(p[i]),color = "Status", line.color = "gray", line.size = 0.6,palette = c("#199E77","#88C540")) + ggtitle(colnames(p[i])) + stat_compare_means(method = "wilcox.test",paired = TRUE)+ theme(text = element_text(color = "black",size=20)) #+ geom_text(aes(label = Sample_ID), size=3)
print(g)
dev.off()
}
for (i in 3:ncol(p2)){
pdf(paste0(pathToGitFolder,"/Figures/Figure4E_",colnames(p2[i]),".pdf"))
g=ggpaired(p2, x = "Status", y = colnames(p2[i]),color = "Status", line.color = "gray", line.size = 0.6,palette = c("#199E77","#88C540")) + ggtitle(colnames(p2[i])) + stat_compare_means(method = "wilcox.test",paired = TRUE)+ theme(text = element_text(color = "black",size=20)) #+ geom_text(aes(label = Sample_ID), size=3)
print(g)
dev.off()
}

#Platelets tests
params=as.data.frame(fread(paste0(pathToGitFolder,"/Tables/platelets_activity_test.csv")))
colnames(params)=c("Status","ID",colnames(params[,3:ncol(params)]))
ind=c(as.character(params$ID[which(params$Status=="Pre")]),paste(unlist(pairs[as.character(params$ID[which(params$Status=="Pre")])])))
rownames(params)=params$ID
p=params[ind,]
p$Status=factor(p$Status,levels=c("Pre","Post"))
p$Status=gsub("Pre","Obese",p$Status)
p$Status=gsub("Post","Post_surgery",p$Status)
set1=c("Status","ID","F_REST_AV_PP","F_ADP_AV_PP","F_CRP_AV_PP","F_TRAP_AV_PP","P_REST_AV_PP","P_ADP_AV_PP","P_CRP_AV_PP","P_TRAP_AV_PP")
set2=c("Status","ID","CD41b","CD49b","CD42b","CD61","CD29","CD9","CD42a","CD36","CD41a")

p1=p[,which(colnames(p) %in% set1)]
p1=p1[complete.cases(p1),]
p2=p[,which(colnames(p) %in% set2)]
p2=p2[complete.cases(p2),]

for (i in 3:ncol(p1)){
p3=p1[,c(1,2,i)]
print(paste0(colnames(p3)[3],":",t.test(p3[1:max(which(p3$Status=="Obese")),3],p3[(max(which(p3$Status=="Obese"))+1):max(which(p3$Status=="Post_surgery")),3] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"F_REST_AV_PP:0.155191691036204"
#"F_ADP_AV_PP:0.363962292209297"
#"F_CRP_AV_PP:0.192963747502714"
#"F_TRAP_AV_PP:0.356656263863601"
#"P_REST_AV_PP:0.695763273425313"
#"P_ADP_AV_PP:0.763017333838875"
#"P_CRP_AV_PP:0.0317812621167331"
#"P_TRAP_AV_PP:0.357150054409674"

for (i in 3:ncol(p2)){
p3=p2[,c(1,2,i)]
print(paste0(colnames(p3)[3],":",t.test(p3[1:max(which(p3$Status=="Obese")),3],p3[(max(which(p3$Status=="Obese"))+1):max(which(p3$Status=="Post_surgery")),3] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"CD41b:0.473054431922429"
#"CD49b:0.140531130708057"
#"CD42b:0.560983150223364"
#"CD61:0.00367324432230169"
#"CD29:0.295766048271454"
#"CD9:0.258415535060441"
#"CD42a:0.362742732704763"
#"CD36:0.0015444726735946"
#"CD41a:0.00784829018640135"


for (i in 3:ncol(p1)){
p3=p1[,c(1,2,i)]
print(paste0(colnames(p3)[3],":",wilcox.test(p3[1:max(which(p3$Status=="Obese")),3],p3[(max(which(p3$Status=="Obese"))+1):max(which(p3$Status=="Post_surgery")),3] , paired = TRUE, alternative = "two.sided")$p.value))
}
#"F_REST_AV_PP:0.1953125"
#"F_ADP_AV_PP:0.546875"
#"F_CRP_AV_PP:0.1953125"
#"F_TRAP_AV_PP:0.25"
#"P_REST_AV_PP:1"
#"P_ADP_AV_PP:0.546875"
#"P_CRP_AV_PP:0.0390625"
#"P_TRAP_AV_PP:0.3828125"


for (i in 3:ncol(p2)){
p3=p2[,c(1,2,i)]
print(paste0(colnames(p3)[3],":",wilcox.test(p3[1:max(which(p3$Status=="Obese")),3],p3[(max(which(p3$Status=="Obese"))+1):max(which(p3$Status=="Post_surgery")),3] , paired = TRUE, alternative = "two.sided")$p.value))
}

#"CD41b:0.46875"
#"CD49b:0.15625"
#"CD42b:0.375"
#"CD61:0.015625"
#"CD29:0.46875"
#"CD9:0.375"
#"CD42a:0.375"
#"CD36:0.015625"
#"CD41a:0.015625"

for (i in 3:ncol(p1)){
pdf(paste0(pathToGitFolder,"/Figures/Figure4E_",colnames(p1[i]),".pdf"))
g=ggpaired(p1, x = "Status", y = colnames(p1[i]),color = "Status", line.color = "gray", line.size = 0.6,palette = c("#199E77","#88C540")) + ggtitle(colnames(p1[i])) + stat_compare_means(method = "t.test",paired = TRUE)+ theme(text = element_text(color = "black",size=20)) #+ geom_text(aes(label = Sample_ID), size=3)
print(g)
dev.off()
}
for (i in 3:ncol(p2)){
pdf(paste0(pathToGitFolder,"/Figures/Figure4E_",colnames(p2[i]),".pdf"))
g=ggpaired(p2, x = "Status", y = colnames(p2[i]),color = "Status", line.color = "gray", line.size = 0.6,palette = c("#199E77","#88C540")) + ggtitle(colnames(p2[i])) + stat_compare_means(method = "t.test",paired = TRUE)+ theme(text = element_text(color = "black",size=20)) #+ geom_text(aes(label = Sample_ID), size=3)
print(g)
dev.off()
}



