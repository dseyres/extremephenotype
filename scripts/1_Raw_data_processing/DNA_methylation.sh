##Transform bismark BAM files into bsmap files for differential methylation analysis using Methylkit



#samtools/0.1.7a  and parallel are REQUIRED

outPath="" ##path to output directory 
bamDir="" ##path to directory containing BAM RRBS files
REF="/path/to/human/reference/genome/Homo_sapiens.GRCh38.fa" #must contain chr prefix

mono_control_IDs=(S01WDG C0T49H S01Y8I C0TUTL S01YAE S01WGA C0UNS6 C0T33X)
mono_control_files=(RRBS_S01WDG51_mono.bam RRBS_C0T49H_mono.bam RRBS_S01Y8I51_mono.bam RRBS_C0TUTL_mono.bam RRBS_S01YAE51_mono.bam RRBS_S01WGA51_mono.bam RRBS_C0UNS6_mono.bam RRBS_C0T33X_mono.bam)

neu_control_IDs=(S01WDG C0T49H S01Y8I C0TUTL S01YAE S01WGA C0UNS6 C0T33X)
neu_control_files=(RRBS_S01WDG52_neutro.bam RRBS_C0T49H_neutro.bam RRBS_S01Y8I52_neutro.bam RRBS_C0TUTL_neutro.bam RRBS_S01YAE52_neutro.bam RRBS_S01WGA52_neutro.bam RRBS_C0UNS6_neutro.bam RRBS_C0T33X_neutro.bam)

macro_control_IDs=(S01WDG C0T49H S01Y8I C0TUTL S01YAE S01WGA C0UNS6 C0T33X)
macro_control_files=(RRBS_S01WDG54_macro.bam RRBS_C0T49H_macro.bam RRBS_S01Y8I54_macro.bam RRBS_C0TUTL_macro.bam RRBS_S01YAE54_macro.bam RRBS_S01WGA54_macro.bam RRBS_C0UNS6_macro.bam RRBS_C0T33X_macro.bam)

mono_pre_IDs=(S01RS6 S01TEQ S01WXD S01XJ0 S022EF S01SYR S01Y9G S022GB S01WFC S01Y7K S022TM S01WCI)
mono_pre_files=(RRBS_S01RS653_mono.bam RRBS_S01TEQ51_mono.bam RRBS_S01WXD51_mono.bam RRBS_S01XJ051_mono.bam RRBS_S022EF_mono.bam RRBS_S01SYR51_mono.bam RRBS_S01Y9G51_mono.bam RRBS_S022GB_mono.bam RRBS_S01WFC51_mono.bam RRBS_S01Y7K51_mono.bam RRBS_S022TM51_mono.bam RRBS_S01WCI51_mono.bam)

neu_pre_IDs=( S01RS6 S01TEQ S01WXD S01XJ0 S022EF S01SYR S01Y9G S022GB S01WFC S01Y7K S022TM S01WCI )
neu_pre_files=( RRBS_S01RS651_neutro.bam RRBS_S01TEQ52_neutro.bam RRBS_S01WXD52_neutro.bam RRBS_S01XJ052_neutro.bam RRBS_S022EF_neutro.bam RRBS_S01SYR53_neutro.bam RRBS_S01Y9G52_neutro.bam RRBS_S022GB_neutro.bam RRBS_S01WFC52_neutro.bam RRBS_S01Y7K52_neutro.bam RRBS_S022TM52_neutro.bam RRBS_S01WCI52_neutro.bam )

macro_pre_IDs=( S01RS6 S01TEQ S01WXD S01XJ0 S022EF S01SYR S01Y9G S022GB S01WFC S01Y7K S022TM S01WCI )
macro_pre_files=( RRBS_S01RS654_macro.bam RRBS_S01TEQ54_macro.bam RRBS_S01WXD54_macro.bam RRBS_S01XJ054_macro.bam RRBS_S022EF_macro.bam RRBS_S01SYR54_macro.bam RRBS_S01Y9G54_macro.bam RRBS_S022GB_macro.bam RRBS_S01WFC54_macro.bam RRBS_S01Y7K54_macro.bam RRBS_S022TM54_macro.bam RRBS_S01WCI54_macro.bam )

##S022EF has no post surgery

mono_post_IDs=( S022QS S0234V S023EB S023RM S0240Z S022UK S0245P S023F9 S023H5 S023PQ S0232Z )
mono_post_files=( RRBS_S022QS_mono.bam RRBS_S0234V_mono.bam RRBS_S023EB53_mono.bam RRBS_S023RM51_mono.bam RRBS_S0240Z51_mono.bam RRBS_S022UK_mono.bam RRBS_S0245P51_mono.bam RRBS_S023F951_mono.bam RRBS_S023H551_mono.bam RRBS_S023PQ51_mono.bam RRBS_S0232Z_mono.bam )

neu_post_IDs=( S022QS S0234V S023EB S023RM S0240Z S022UK S0245P S023F9 S023H5 S023PQ S0232Z )
neu_post_files=( RRBS_S022QS_neutro.bam RRBS_S0234V_neutro.bam RRBS_S023EB52_neutro.bam RRBS_S023RM52_neutro.bam RRBS_S0240Z52_neutro.bam RRBS_S022UK_neutro.bam RRBS_S0245P52_neutro.bam RRBS_S023F953_neutro.bam RRBS_S023H552_neutro.bam RRBS_S023PQ52_neutro.bam RRBS_S0232Z_neutro.bam )

macro_post_IDs=( S022QS S0234V S023EB S023RM S0240Z S022UK S0245P S023F9 S023H5 S023PQ S0232Z )
macro_post_files=( RRBS_S022QS_macro.bam RRBS_S0234V_macro.bam RRBS_S023EB54_macro.bam RRBS_S023RM54_macro.bam RRBS_S0240Z54_macro.bam RRBS_S022UK_macro.bam RRBS_S0245P54_macro.bam RRBS_S023F954_macro.bam RRBS_S023H554_macro.bam RRBS_S023PQ54_macro.bam RRBS_S0232Z_macro.bam )

mono_lipo_IDs=( C0NGW8 C0NGX6 C0RFVJ C0T34V C0T4E7 C0TUWF C0UNU2 C0Y7VW C0YAVK C0SJS1 )
mono_lipo_files=( RRBS_C0NGW8_mono.bam RRBS_C0NGX6_mono.bam RRBS_C0RFVJ_mono.bam RRBS_C0T34V_mono.bam RRBS_C0T4E7_mono.bam RRBS_C0TUWF_mono.bam RRBS_C0UNU2_mono.bam RRBS_C0Y7VW_mono.bam RRBS_C0YAVK_mono.bam RRBS_C0SJS1_mono.bam )

neu_lipo_IDs=( C0NGW8 C0NGX6 C0RFVJ C0T34V C0T4E7 C0TUWF C0UNU2 C0Y7VW C0YAVK C0SJS1 )
neu_lipo_files=( RRBS_C0NGW8_neutro.bam RRBS_C0NGX6_neutro.bam RRBS_C0RFVJ_neutro.bam RRBS_C0T34V_neutro.bam RRBS_C0T4E7_neutro.bam RRBS_C0TUWF_neutro.bam RRBS_C0UNU2_neutro.bam RRBS_C0Y7VW_neutro.bam RRBS_C0YAVK_neutro.bam RRBS_C0SJS1_neutro.bam)

macro_lipo_IDs=( C0NGW8 C0NGX6 C0RFVJ C0T34V C0T4E7 C0TUWF C0UNU2 C0Y7VW C0YAVK C0SJS1 )
macro_lipo_files=( RRBS_C0NGW8_macro.bam RRBS_C0NGX6_macro.bam RRBS_C0RFVJ_macro.bam RRBS_C0T34V_macro.bam RRBS_C0T4E7_macro.bam RRBS_C0TUWF_macro.bam RRBS_C0UNU2_macro.bam RRBS_C0Y7VW_macro.bam RRBS_C0YAVK_macro.bam RRBS_C0SJS1_macro.bam )


##extract methylation informations from bsmap BAM file with methratio.py script (https://github.com/genome-vendor/bsmap)
#Uses 12 threads, can be reduced or increased

#Monocytes
mono_pre_files_fn=( "${mono_pre_files[@]/.bam/}" )
mono_post_files_fn=( "${mono_post_files[@]/.bam/}" )
mono_control_files_fn=( "${mono_control_files[@]/.bam/}" )
mono_lipo_files_fn=( "${mono_lipo_files[@]/.bam/}" )

parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}{}_bsmap.txt -d ${REF} ${bamDir}{}.bam" ::: ${mono_pre_files_fn[@]} 
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${mono_post_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${mono_control_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${mono_lipo_files_fn[@]}

#Neutrophils
neu_pre_files_fn=( "${neu_pre_files[@]/.bam/}" )
neu_post_files_fn=( "${neu_post_files[@]/.bam/}" )
neu_control_files_fn=( "${neu_control_files[@]/.bam/}" )
neu_lipo_files_fn=( "${neu_lipo_files[@]/.bam/}" )

parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}{}_bsmap.txt -d ${REF} ${bamDir}{}.bam" ::: ${neu_pre_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${neu_post_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${neu_control_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${neu_lipo_files_fn[@]}

#Macrophages
macro_pre_files_fn=( "${macro_pre_files[@]/.bam/}" )
macro_post_files_fn=( "${macro_post_files[@]/.bam/}" )
macro_control_files_fn=( "${macro_control_files[@]/.bam/}" )
macro_lipo_files_fn=( "${macro_lipo_files[@]/.bam/}" )

parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}{}_bsmap.txt -d ${REF} ${bamDir}{}.bam" ::: ${macro_pre_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${macro_post_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${macro_control_files_fn[@]}
parallel -j12 "python ~/scripts/bsmap-master/methratio.py -o ${outPath}/{}_bsmap.txt -d ${REF} ${bamDir}/{}.bam" ::: ${macro_lipo_files_fn[@]}

   
##Command line if you desire to separate Cs by context: CpGs, CHH, CHG

#awk '(NR>1){if(($3=="-" && $4~/^.CG../ ) || ($3=="+" && $4~/^..CG./)) print $1"\t"$2-1"\t"$2"\t"$3"\t""CG""\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12; else if(($3=="-" && $4~/^C[AGT]G../ ) || ($3=="+" && $4~/^..C[ACT]G/)) print $1"\t"$2-1"\t"$2"\t"$3"\t""CHG""\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12; else if(($3=="-" && $4~/^[AGT][AGT]G../ ) || ($3=="+" && $4~/^..C[ACT][ACT]/)) print $1"\t"$2-1"\t"$2"\t"$3"\t""CHH""\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12; else print $1"\t"$2-1"\t"$2"\t"$3"\t""CNN""\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12}' BSMAPratio/infile.txt > BSMAPratio/BSMAP_outfile.txt

for file in ${outPath}/*_bsmap.txt ; do fn=$(basename ${file} _bsmap.txt) ; grep -v 'chrX\|chrY' $file > ${fn}_noXY_bsmap.txt ; done



