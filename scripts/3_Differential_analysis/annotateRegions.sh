##Annotate peaks and CpGs
#this script requires HOMER tool suite to be installed with hg38 reference genome
#wget http://homer.ucsd.edu/homer/configureHomer.pl
# configureHomer.pl -install hg38

type=$1 #"chip" or "RRBS" 
filename=$2 #filename without extension
pathToGitFolder=""
tmpPath=${pathToGitFolder} ##specify tmp path here

pathToHomer="path/to/homer/bin"

mkdir -p ${pathToGitFolder}/HOMER

if [ ${type} == "chip" ]
	then
	outputDir=${pathToGitFolder}/HOMER
	##Add chr to chromosome name
	sed 's/^/chr/' ${filename}.bed > ${tmpPath}/${filename}_UCSC.bed
	perl ${pathToHomer}/annotatePeaks.pl ${tmpPath}/${filename}_UCSC.bed hg38 -annStats ${outputDir}/${filename}.stats.txt > ${outputDir}/${filename}.csv
		
elif [ ${type} == "RRBS" ]
	then
	outputDir=${pathToGitFolder}/HOMER
	##transform RRBS csv positions into bed like file. 
	cut -f2-5 -d"," ${filename}.csv > ${tmpPath}/${filename}_RRBS.bed
	sed -i 's/,/\t/g' ${tmpPath}/${filename}_RRBS.bed
	sed -i 's/"//g' ${tmpPath}/${filename}_RRBS.bed
	tail -n +2 ${tmpPath}/${filename}_RRBS.bed > ${tmpPath}/${filename}_RRBS2.bed
	mv ${tmpPath}/${filename}_RRBS2.bed ${tmpPath}/${filename}_RRBS.bed
	perl ${pathToHomer}/annotatePeaks.pl ${tmpPath}/${filename}_RRBS.bed hg38 -annStats ${outputDir}/${filename}.stats.txt > ${outputDir}/${filename}.txt
	
fi

Rscript ${pathToGitFolder}/scripts/3_Differential_analysis/annotateRegions.R ${filename} ${outputDir}

