rm(list = ls())

library(ggplot2)
library(DiffBind)
library(pheatmap)
library(ggExtra)
library(data.table)

#Before running this script, you need to update paths to peak and BAM files in the manifest file. Follow guidelines to produce these files from raw fastqs (downloaded from EGA)

FDR=0.05  #diffbind threshold
minOverlap=0.5  #diffbind parameter
max_peak_width=0  #maximum merged peak width ; set to 0 for no filtering
FRiP_filter=TRUE #apply threshold on FRiP score
min_FRiP=0.05 #minimum FRiP score
min_number_of_reads=10  #minimum number of reads to support a merged peak; set to 0 for no filtering
pipelineScore=-3 # threshold for qualtiy assessment score

pathToGitFolder=""
source(paste0(pathToGitFolder,"/scripts/2_Data_Post_processing/functions.R"))
source(paste0(pathToGitFolder,"/scripts/3_Differential_analysis/Lean_selection.R"))
manifest=paste0(pathToGitFolder,"/Tables/ChIPseqPipelineScores.csv")
col.diffbind=c("SampleID","Tissue","Factor","Condition","Replicate","bamReads","bamControl","Peaks","PeakCaller","xIntercept","elbowPoint","ScoreFilter")
covariates=fread(paste0(pathToGitFolder,"/Tables/anthropometric_values.csv"))
covs=c("ID","GENDER","WEIGHT","AGE","LAR","GLC","TC","TG","HDL","LDL","ALT","AST","hsCRP","HOMA_IR","AT_IR")
covariates=as.data.frame(covariates)[,covs]
rownames(covariates)=covariates$ID
covariates$ID=NULL
celltypes=c("Neu","Mono","Macro")

for (celltype in celltypes)
{
	print(paste0("Analysing: ",celltype))

	########################################################################
	########################################################################
	########################################################################
	print("Compare Obese_vs_Post_surgery")
	comp="Obese_vs_Post_surgery"
	samples <- as.data.frame(fread(manifest))
	samples=merge(samples,covariates,by="row.names",all.x=TRUE,all.y=FALSE)
	samples=droplevels(samples)
	samples=samples[which(samples$ScoreFilter>=pipelineScore),]
	samples = samples[grep(celltype, samples$Tissue),]
	samples.diffbind=samples[colnames(samples) %in% col.diffbind]
	samples.diffbind.conditions = samples.diffbind[grep("Obese|Post",samples.diffbind$Condition),]
	samples.diffbind.conditions$Replicate=as.factor(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Replicate=droplevels(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Condition=as.factor(samples.diffbind.conditions$Condition)
	samples.diffbind.conditions$Condition=droplevels(samples.diffbind.conditions$Condition)
	colnames(samples.diffbind.conditions)=gsub("ScoreFilter","Treatment",colnames(samples.diffbind.conditions))
	samples.diffbind.conditions$Treatment=as.factor(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions$Treatment=droplevels(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions[,c("ScoreFilter","elbowPoint","xIntercept")]<- list(NULL)
	pairs=names(which(table(samples.diffbind.conditions$Replicate)==2))
	samples.diffbind.conditions=samples.diffbind.conditions[which(samples.diffbind.conditions$Replicate %in% pairs),]
	print(dim(samples.diffbind.conditions))

	##Remove sex chromosomes
	d=c()
	for (i in 1:nrow(samples.diffbind.conditions)){
		a=read.table(as.character(samples.diffbind.conditions$Peaks[i]))
		a=a[!grepl("X|Y",a$V1),]
		write.table(a,file=paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
		d=c(d,paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
	}
	samples.diffbind.conditions$Peaks=d

	samples.diffbind.conditions.cur=samples.diffbind.conditions

	anninfo<-subset(samples.diffbind.conditions.cur, select = c(Condition,SampleID))
	anninfo$Condition=as.factor(anninfo$Condition)
	rownames(anninfo)<- anninfo$SampleID
	anninfo<-subset(anninfo, select=-SampleID)
	colnames(anninfo)<- c("Condition")
	an_colors = list("Condition" = c('Obese'="#46ed46", 'Post_surgery' = "#f8c851"))
	samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
	samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 

	#FRiP filtering:
	lowFRiP_samples=c(as.character(samples.diffbind.conditions.dba$samples[which(samples.diffbind.conditions.dba$SN<min_FRiP),]$SampleID))

	if ((FRiP_filter==TRUE) & (length(lowFRiP_samples)>0)){
		samples.diffbind.conditions.cur=subset(samples.diffbind.conditions.cur, !(samples.diffbind.conditions.cur$SampleID %in% lowFRiP_samples))
		pairs=samples.diffbind.conditions.cur$Replicate[duplicated(samples.diffbind.conditions.cur$Replicate)]
		samples.diffbind.conditions.cur=samples.diffbind.conditions.cur[which(samples.diffbind.conditions.cur$Replicate %in% pairs),]
		samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
		samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 
	}

	samples.diffbind.conditions.dba <- dba.contrast(samples.diffbind.conditions.dba,categories=DBA_CONDITION,block=DBA_REPLICATE,minMembers=2)
	samples.diffbind.conditions.dba=diffbind_analysis(samples.diffbind.conditions.cur,anninfo,an_colors,celltype,comp,FDR,samples.diffbind.conditions.dba,block=T,minOverlap=minOverlap,max_peak_width=max_peak_width,dba=samples.diffbind.conditions.dba,min_number_of_reads=min_number_of_reads)
	diffbindPlot(samples.diffbind.conditions.dba=samples.diffbind.conditions.dba,anninfo=anninfo,an_colors=an_colors,celltype=celltype,comp=comp)

	##########################################################
	##########################################################
	##########################################################

	print("Compare Obese_vs_Lean")

	comp="Obese_vs_Lean"
	samples <- as.data.frame(fread(manifest))
	samples=droplevels(samples)
	samples=samples[which(samples$ScoreFilter>=pipelineScore),]
	samples = samples[grep(celltype, samples$Tissue),]
	samples.diffbind=samples[colnames(samples) %in% col.diffbind]
	samples.diffbind.cont = samples.diffbind[grep("Lean",samples.diffbind$Condition),]
	samples.diffbind.cont=samples.diffbind.cont[samples.diffbind.cont$Replicate %in% selected_donors,]
	samples.diffbind.pre = samples.diffbind[grep("Obese",samples.diffbind$Condition),]
	samples.diffbind.conditions=rbind(samples.diffbind.cont,samples.diffbind.pre)
	rownames(samples.diffbind.conditions)=samples.diffbind.conditions$Replicate
	samples.diffbind.conditions=merge(samples.diffbind.conditions,covariates,by="row.names",all.x=TRUE,all.y=FALSE)
	samples.diffbind.conditions$Replicate=as.factor(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Replicate=droplevels(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Condition=as.factor(samples.diffbind.conditions$Condition)
	samples.diffbind.conditions$Condition=droplevels(samples.diffbind.conditions$Condition)
	colnames(samples.diffbind.conditions)=gsub("ScoreFilter","Treatment",colnames(samples.diffbind.conditions))
	samples.diffbind.conditions$Treatment=as.factor(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions$Treatment=droplevels(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions[,c("ScoreFilter","elbowPoint","xIntercept", "WEIGHT","LAR","GLC","TC","TG","HDL","LDL","ALT","AST","hsCRP","HOMA_IR","AT_IR")]<- list(NULL)
	samples.diffbind.conditions$Factor=samples.diffbind.conditions$GENDER
	samples.diffbind.conditions$Tissue=samples.diffbind.conditions$AGE
	samples.diffbind.conditions=samples.diffbind.conditions[complete.cases(samples.diffbind.conditions),]

	##Remove sex chromosomes
	d=c()
	for (i in 1:nrow(samples.diffbind.conditions)){
		a=read.table(as.character(samples.diffbind.conditions$Peaks[i]))
		a=a[!grepl("X|Y",a$V1),]
		write.table(a,file=paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
		d=c(d,paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
	}
	samples.diffbind.conditions$Peaks=d
	samples.diffbind.conditions.cur=samples.diffbind.conditions
	anninfo<-subset(samples.diffbind.conditions.cur, select = c(Condition,SampleID))
	anninfo$Condition=as.factor(anninfo$Condition)
	rownames(anninfo)<- anninfo$SampleID
	anninfo<-subset(anninfo, select=-SampleID)
	colnames(anninfo)<- c("Condition")
	an_colors = list("Condition" = c('Obese'="#46ed46", 'Lean' = "#f8c851"))
	samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
	samples.diffbind.conditions.dba=dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 

	#FRiP filtering:
	lowFRiP_samples=c(as.character(samples.diffbind.conditions.dba$samples[which(samples.diffbind.conditions.dba$SN<min_FRiP),]$SampleID))

	if ((FRiP_filter==TRUE) & (length(lowFRiP_samples)>0)){
		samples.diffbind.conditions.cur=subset(samples.diffbind.conditions.cur, !(samples.diffbind.conditions.cur$SampleID %in% lowFRiP_samples))
		samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
		samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 
	}

	samples.diffbind.conditions.dba <- dba.contrast(samples.diffbind.conditions.dba,categories=DBA_CONDITION,block=c(DBA_FACTOR,DBA_TISSUE),minMembers=2)
	samples.diffbind.conditions.dba=diffbind_analysis(samples.diffbind.conditions.cur,anninfo,an_colors,celltype,comp,FDR,samples.diffbind.conditions.dba,block=T,minOverlap=minOverlap,max_peak_width=max_peak_width,dba=samples.diffbind.conditions.dba,min_number_of_reads=min_number_of_reads)
	diffbindPlot(samples.diffbind.conditions.dba=samples.diffbind.conditions.dba,anninfo=anninfo,an_colors=an_colors,celltype=celltype,comp=comp)

	##########################################################
	##########################################################
	##########################################################

	print("Compare Lipo_vs_Lean")

	comp="Lipo_vs_Lean"
	samples <- as.data.frame(fread(manifest))
	samples=droplevels(samples)
	samples=samples[which(samples$ScoreFilter>=pipelineScore),]
	samples = samples[grep(celltype, samples$Tissue),]
	samples.diffbind=samples[colnames(samples) %in% col.diffbind]
	samples.diffbind.cont = samples.diffbind[grep("Lean",samples.diffbind$Condition),]
	samples.diffbind.cont=samples.diffbind.cont[samples.diffbind.cont$Replicate %in% selected_donors,]
	samples.diffbind.pre = samples.diffbind[grep("Lipo",samples.diffbind$Condition),]
	samples.diffbind.conditions=rbind(samples.diffbind.cont,samples.diffbind.pre)
	rownames(samples.diffbind.conditions)=samples.diffbind.conditions$Replicate
	samples.diffbind.conditions=merge(samples.diffbind.conditions,covariates,by="row.names",all.x=TRUE,all.y=FALSE)
	samples.diffbind.conditions$Replicate=as.factor(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Replicate=droplevels(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Condition=as.factor(samples.diffbind.conditions$Condition)
	samples.diffbind.conditions$Condition=droplevels(samples.diffbind.conditions$Condition)
	colnames(samples.diffbind.conditions)=gsub("ScoreFilter","Treatment",colnames(samples.diffbind.conditions))
	samples.diffbind.conditions$Treatment=as.factor(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions$Treatment=droplevels(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions[,c("ScoreFilter","elbowPoint","xIntercept", "WEIGHT","LAR","GLC","TC","TG","HDL","LDL","ALT","AST","hsCRP","HOMA_IR","AT_IR")]<- list(NULL)
	samples.diffbind.conditions$Factor=samples.diffbind.conditions$GENDER
	samples.diffbind.conditions$Tissue=samples.diffbind.conditions$AGE
	samples.diffbind.conditions=samples.diffbind.conditions[complete.cases(samples.diffbind.conditions),]


	##Remove sex chromosomes
	d=c()
	for (i in 1:nrow(samples.diffbind.conditions)){
		a=read.table(as.character(samples.diffbind.conditions$Peaks[i]))
		a=a[!grepl("X|Y",a$V1),]
		write.table(a,file=paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
		d=c(d,paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
	}
	samples.diffbind.conditions$Peaks=d
	samples.diffbind.conditions.cur=samples.diffbind.conditions

	anninfo<-subset(samples.diffbind.conditions.cur, select = c(Condition,SampleID))
	anninfo$Condition=as.factor(anninfo$Condition)
	rownames(anninfo)<- anninfo$SampleID
	anninfo<-subset(anninfo, select=-SampleID)
	colnames(anninfo)<- c("Condition")
	an_colors = list("Condition" = c('Lipodystrophy'="#46ed46", 'Lean' = "#f8c851"))
	samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
	samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 

	#FRiP filtering:
	lowFRiP_samples=c(as.character(samples.diffbind.conditions.dba$samples[which(samples.diffbind.conditions.dba$SN<min_FRiP),]$SampleID))

	if ((FRiP_filter==TRUE) & (length(lowFRiP_samples)>0)){
		samples.diffbind.conditions.cur=subset(samples.diffbind.conditions.cur, !(samples.diffbind.conditions.cur$SampleID %in% lowFRiP_samples))
		samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
		samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 
	}

	samples.diffbind.conditions.dba <- dba.contrast(samples.diffbind.conditions.dba,categories=DBA_CONDITION,block=c(DBA_FACTOR,DBA_TISSUE),minMembers=2)
	samples.diffbind.conditions.dba=diffbind_analysis(samples.diffbind.conditions.cur,anninfo,an_colors,celltype,comp,FDR,samples.diffbind.conditions.dba,block=T,minOverlap=minOverlap,max_peak_width=max_peak_width,dba=samples.diffbind.conditions.dba,min_number_of_reads=min_number_of_reads)
	diffbindPlot(samples.diffbind.conditions.dba=samples.diffbind.conditions.dba,anninfo=anninfo,an_colors=an_colors,celltype=celltype,comp=comp)

	##########################################################
	##########################################################
	##########################################################

	print("Compare Lipo_vs_Obese")

	comp="Lipo_vs_Obese"
	samples <- as.data.frame(fread(manifest))
	samples=droplevels(samples)
	samples=samples[which(samples$ScoreFilter>=pipelineScore),]
	samples = samples[grep(celltype, samples$Tissue),]
	samples.diffbind=samples[colnames(samples) %in% col.diffbind]
	samples.diffbind.cont = samples.diffbind[grep("Lipodystrophy",samples.diffbind$Condition),]
	samples.diffbind.pre = samples.diffbind[grep("Obese",samples.diffbind$Condition),]
	samples.diffbind.conditions=rbind(samples.diffbind.cont,samples.diffbind.pre)
	rownames(samples.diffbind.conditions)=samples.diffbind.conditions$Replicate
	samples.diffbind.conditions=merge(samples.diffbind.conditions,covariates,by="row.names",all.x=TRUE,all.y=FALSE)
	samples.diffbind.conditions$Replicate=as.factor(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Replicate=droplevels(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Condition=as.factor(samples.diffbind.conditions$Condition)
	samples.diffbind.conditions$Condition=droplevels(samples.diffbind.conditions$Condition)
	colnames(samples.diffbind.conditions)=gsub("ScoreFilter","Treatment",colnames(samples.diffbind.conditions))
	samples.diffbind.conditions$Treatment=as.factor(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions$Treatment=droplevels(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions[,c("ScoreFilter","elbowPoint","xIntercept", "WEIGHT","LAR","GLC","TC","TG","HDL","LDL","ALT","AST","hsCRP","HOMA_IR","AT_IR")]<- list(NULL)
	samples.diffbind.conditions$Factor=samples.diffbind.conditions$GENDER
	samples.diffbind.conditions$Tissue=samples.diffbind.conditions$AGE
	samples.diffbind.conditions=samples.diffbind.conditions[complete.cases(samples.diffbind.conditions),]


	##Remove sex chromosomes
	d=c()
	for (i in 1:nrow(samples.diffbind.conditions)){
		a=read.table(as.character(samples.diffbind.conditions$Peaks[i]))
		a=a[!grepl("X|Y",a$V1),]
		write.table(a,file=paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
		d=c(d,paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
	}
	samples.diffbind.conditions$Peaks=d
	samples.diffbind.conditions.cur=samples.diffbind.conditions

	anninfo<-subset(samples.diffbind.conditions.cur, select = c(Condition,SampleID))
	anninfo$Condition=as.factor(anninfo$Condition)
	rownames(anninfo)<- anninfo$SampleID
	anninfo<-subset(anninfo, select=-SampleID)
	colnames(anninfo)<- c("Condition")
	an_colors = list("Condition" = c('Obese'="#46ed46", 'Lipodystrophy' = "#f8c851"))
	samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
	samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 

	#FRiP filtering:
	lowFRiP_samples=c(as.character(samples.diffbind.conditions.dba$samples[which(samples.diffbind.conditions.dba$SN<min_FRiP),]$SampleID))

	if ((FRiP_filter==TRUE) & (length(lowFRiP_samples)>0)){
		samples.diffbind.conditions.cur=subset(samples.diffbind.conditions.cur, !(samples.diffbind.conditions.cur$SampleID %in% lowFRiP_samples))
		samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
		samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 
	}

	samples.diffbind.conditions.dba <- dba.contrast(samples.diffbind.conditions.dba,categories=DBA_CONDITION,block=c(DBA_FACTOR,DBA_TISSUE),minMembers=2)
	samples.diffbind.conditions.dba=diffbind_analysis(samples.diffbind.conditions.cur,anninfo,an_colors,celltype,comp,FDR,samples.diffbind.conditions.dba,block=T,minOverlap=minOverlap,max_peak_width=max_peak_width,dba=samples.diffbind.conditions.dba,min_number_of_reads=min_number_of_reads)
	diffbindPlot(samples.diffbind.conditions.dba=samples.diffbind.conditions.dba,anninfo=anninfo,an_colors=an_colors,celltype=celltype,comp=comp)


	##########################################################
	##########################################################
	##########################################################

	print("Compare Post_surgery_vs_Lean")

	comp="Post_surgery_vs_Lean"
	samples <- as.data.frame(fread(manifest))
	samples=droplevels(samples)
	samples=samples[which(samples$ScoreFilter>=pipelineScore),]
	samples = samples[grep(celltype, samples$Tissue),]
	samples.diffbind=samples[colnames(samples) %in% col.diffbind]
	samples.diffbind.cont = samples.diffbind[grep("Lean",samples.diffbind$Condition),]
	samples.diffbind.cont=samples.diffbind.cont[samples.diffbind.cont$Replicate %in% selected_donors,]
	samples.diffbind.pre = samples.diffbind[grep("Post",samples.diffbind$Condition),]
	samples.diffbind.conditions=rbind(samples.diffbind.cont,samples.diffbind.pre)
	rownames(samples.diffbind.conditions)=samples.diffbind.conditions$Replicate
	samples.diffbind.conditions=merge(samples.diffbind.conditions,covariates,by="row.names",all.x=TRUE,all.y=FALSE)
	samples.diffbind.conditions$Replicate=as.factor(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Replicate=droplevels(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Condition=as.factor(samples.diffbind.conditions$Condition)
	samples.diffbind.conditions$Condition=droplevels(samples.diffbind.conditions$Condition)
	colnames(samples.diffbind.conditions)=gsub("ScoreFilter","Treatment",colnames(samples.diffbind.conditions))
	samples.diffbind.conditions$Treatment=as.factor(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions$Treatment=droplevels(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions[,c("ScoreFilter","elbowPoint","xIntercept", "WEIGHT","LAR","GLC","TC","TG","HDL","LDL","ALT","AST","hsCRP","HOMA_IR","AT_IR")]<- list(NULL)
	samples.diffbind.conditions$Factor=samples.diffbind.conditions$GENDER
	samples.diffbind.conditions$Tissue=samples.diffbind.conditions$AGE
	samples.diffbind.conditions=samples.diffbind.conditions[complete.cases(samples.diffbind.conditions),]


	##Remove sex chromosomes
	d=c()
	for (i in 1:nrow(samples.diffbind.conditions)){
		a=read.table(as.character(samples.diffbind.conditions$Peaks[i]))
		a=a[!grepl("X|Y",a$V1),]
		write.table(a,file=paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
		d=c(d,paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
	}
	samples.diffbind.conditions$Peaks=d
	samples.diffbind.conditions.cur=samples.diffbind.conditions

	anninfo<-subset(samples.diffbind.conditions.cur, select = c(Condition,SampleID))
	anninfo$Condition=as.factor(anninfo$Condition)
	rownames(anninfo)<- anninfo$SampleID
	anninfo<-subset(anninfo, select=-SampleID)
	colnames(anninfo)<- c("Condition")
	an_colors = list("Condition" = c('Post_surgery'="#46ed46", 'Lean' = "#f8c851"))
	samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
	samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 

	#FRiP filtering:
	lowFRiP_samples=c(as.character(samples.diffbind.conditions.dba$samples[which(samples.diffbind.conditions.dba$SN<min_FRiP),]$SampleID))

	if ((FRiP_filter==TRUE) & (length(lowFRiP_samples)>0)){
		samples.diffbind.conditions.cur=subset(samples.diffbind.conditions.cur, !(samples.diffbind.conditions.cur$SampleID %in% lowFRiP_samples))
		samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
		samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 
	}

	samples.diffbind.conditions.dba <- dba.contrast(samples.diffbind.conditions.dba,categories=DBA_CONDITION,block=c(DBA_FACTOR,DBA_TISSUE),minMembers=2)
	samples.diffbind.conditions.dba=diffbind_analysis(samples.diffbind.conditions.cur,anninfo,an_colors,celltype,comp,FDR,samples.diffbind.conditions.dba,block=T,minOverlap=minOverlap,max_peak_width=max_peak_width,dba=samples.diffbind.conditions.dba,min_number_of_reads=min_number_of_reads)
	diffbindPlot(samples.diffbind.conditions.dba=samples.diffbind.conditions.dba,anninfo=anninfo,an_colors=an_colors,celltype=celltype,comp=comp)


	##########################################################
	##########################################################
	##########################################################

	print("Compare Post_surgery_vs_Lipo")

	comp="Post_surgery_vs_Lipo"
	samples <- as.data.frame(fread(manifest))
	samples=droplevels(samples)
	samples=samples[which(samples$ScoreFilter>=pipelineScore),]
	samples = samples[grep(celltype, samples$Tissue),]
	samples.diffbind=samples[colnames(samples) %in% col.diffbind]
	samples.diffbind.cont = samples.diffbind[grep("Lipodystrophy",samples.diffbind$Condition),]
	samples.diffbind.pre = samples.diffbind[grep("Post",samples.diffbind$Condition),]
	samples.diffbind.conditions=rbind(samples.diffbind.cont,samples.diffbind.pre)
	rownames(samples.diffbind.conditions)=samples.diffbind.conditions$Replicate
	samples.diffbind.conditions=merge(samples.diffbind.conditions,covariates,by="row.names",all.x=TRUE,all.y=FALSE)
	samples.diffbind.conditions$Replicate=as.factor(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Replicate=droplevels(samples.diffbind.conditions$Replicate)
	samples.diffbind.conditions$Condition=as.factor(samples.diffbind.conditions$Condition)
	samples.diffbind.conditions$Condition=droplevels(samples.diffbind.conditions$Condition)
	colnames(samples.diffbind.conditions)=gsub("ScoreFilter","Treatment",colnames(samples.diffbind.conditions))
	samples.diffbind.conditions$Treatment=as.factor(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions$Treatment=droplevels(samples.diffbind.conditions$Treatment)
	samples.diffbind.conditions[,c("ScoreFilter","elbowPoint","xIntercept", "WEIGHT","LAR","GLC","TC","TG","HDL","LDL","ALT","AST","hsCRP","HOMA_IR","AT_IR")]<- list(NULL)
	samples.diffbind.conditions$Factor=samples.diffbind.conditions$GENDER
	samples.diffbind.conditions$Tissue=samples.diffbind.conditions$AGE
	samples.diffbind.conditions=samples.diffbind.conditions[complete.cases(samples.diffbind.conditions),]


	##Remove sex chromosomes
	d=c()
	for (i in 1:nrow(samples.diffbind.conditions)){
		a=read.table(as.character(samples.diffbind.conditions$Peaks[i]))
		a=a[!grepl("X|Y",a$V1),]
		write.table(a,file=paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
		d=c(d,paste0(pathToGitFolder,"/",basename(as.character(samples.diffbind.conditions$Peaks[i]))))
	}
	samples.diffbind.conditions$Peaks=d
	samples.diffbind.conditions.cur=samples.diffbind.conditions

	anninfo<-subset(samples.diffbind.conditions.cur, select = c(Condition,SampleID))
	anninfo$Condition=as.factor(anninfo$Condition)
	rownames(anninfo)<- anninfo$SampleID
	anninfo<-subset(anninfo, select=-SampleID)
	colnames(anninfo)<- c("Condition")
	an_colors = list("Condition" = c('Post_surgery'="#46ed46", 'Lipodystrophy' = "#f8c851"))
	samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
	samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 

	#FRiP filtering:
	lowFRiP_samples=c(as.character(samples.diffbind.conditions.dba$samples[which(samples.diffbind.conditions.dba$SN<min_FRiP),]$SampleID))

	if ((FRiP_filter==TRUE) & (length(lowFRiP_samples)>0)){
		samples.diffbind.conditions.cur=subset(samples.diffbind.conditions.cur, !(samples.diffbind.conditions.cur$SampleID %in% lowFRiP_samples))
		samples.diffbind.conditions.dba=dba(sampleSheet=samples.diffbind.conditions.cur)
		samples.diffbind.conditions.dba <- dba.count(samples.diffbind.conditions.dba,minOverlap=minOverlap) 
	}

	samples.diffbind.conditions.dba <- dba.contrast(samples.diffbind.conditions.dba,categories=DBA_CONDITION,block=c(DBA_FACTOR,DBA_TISSUE),minMembers=2)
	samples.diffbind.conditions.dba=diffbind_analysis(samples.diffbind.conditions.cur,anninfo,an_colors,celltype,comp,FDR,samples.diffbind.conditions.dba,block=T,minOverlap=minOverlap,max_peak_width=max_peak_width,dba=samples.diffbind.conditions.dba,min_number_of_reads=min_number_of_reads)
	diffbindPlot(samples.diffbind.conditions.dba=samples.diffbind.conditions.dba,anninfo=anninfo,an_colors=an_colors,celltype=celltype,comp=comp)

}