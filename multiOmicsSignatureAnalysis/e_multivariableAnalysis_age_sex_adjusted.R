##############################################################################################
##################################### Variable selection #####################################
##############################################################################################

rm(list=ls())

library(gdata) # for read.xls()
library(ggplot2) 
library(gridExtra) # for grid.arrange()
#library(cowplot)
library(glmnet)
library(sfsmisc)
library(pheatmap)

# Load scaled datasets
load("processedOmicsData/data_scaled.RData")
# Provides: "dataset_names"  "intersectIDs"   "scaled_dataset" "unionIDs"
# where: 
#    scaled_dataset : list containing all datasets (after centring and scaling)
#    dataset_names  : names associated with each dataset in the scaled_dataset list
#    intersectIDs   : IDs of individuals for whom we have all omics layers
#    unionIDs       : IDs of individuals for whom we have at least one omic layer
load("processedOmicsData/annotations.RData")
# Provides: "response"
# where:
#    response       : contains clinical information about all individuals, but we 
#                     only use response$COHORT, which indicates whether an individual
#                     is  Control, Lipodystrophy, Obese, WP10_donor
##    Note that:
##       > table(response$COHORT)
##       
##       Control Lipodystrophy         Obese    WP10_donor 
##            15            10            11           187 

#Also load the clinical data, so that we can adjust for age and sex:
load("processedClinicalData/imputedClinicalNew.RData")
## provides completedClinicalData
completedClinicalData$SEX <- varhandle::to.dummy(completedClinicalData$GENDER, "binary")[,1]


## Define function to select variables using glmnet
selectVariables_adjustingForAgeAndSex <- function(k, training_X, training_y, penaltyFactor) {
  set.seed(k)
  fittedGlmnet   <- cv.glmnet(training_X, as.factor(training_y), family = "binomial",
                              alpha = alpha, standardize = FALSE, penalty.factor = penaltyFactor)
}

## Define function to return the number of selected variables
numberOfSelectedVariables <- function(fittedModel) {
  nSelections <- (length(which(coef(fittedModel, s= "lambda.min")!=0))-1)
}

## Define function to return the coefficients of the variables
coefficientsOfVariables <- function(fittedModel) {
  fittedCoefs <- coef(fittedModel,  s = "lambda.min")
}

## Set params for biocparallel
params <- BiocParallel::bpparam()
BiocParallel::bpprogressbar(params) <- TRUE
BiocParallel::bptasks(params)       <- 250


data         <- scaled_dataset
n_data_types <- length(data)
dir.create("results_multivariableAnalysis", showWarnings = FALSE)

comparisons <- c("patients_controls", "bariatric_controls", "lipodystrophy_controls")


for(comparison in comparisons){
  dir.create(paste0("results_multivariableAnalysis/", comparison), showWarnings = FALSE)
  
  for(dataset_name in dataset_names){
    
    cat(which(dataset_names == dataset_name), "/ 8:", dataset_name, "\n")
    currentLayer          <- data[[which(dataset_names == dataset_name)]]
    
    control_IDS           <- rownames(response)[which(response$COHORT == "Control")]
    obese_IDS             <- rownames(response)[which(response$COHORT == "Obese")]
    lipo_IDS              <- rownames(response)[which(response$COHORT == "Lipodystrophy")]
    
    currentLayer_controls <- currentLayer[which(rownames(currentLayer) %in% control_IDS),]
    currentLayer_obese    <- currentLayer[which(rownames(currentLayer) %in% obese_IDS),]
    currentLayer_lipo     <- currentLayer[which(rownames(currentLayer) %in% lipo_IDS),]
    
    clinicalData_controls <- completedClinicalData[rownames(currentLayer_controls),]
    clinicalData_obese    <- completedClinicalData[rownames(currentLayer_obese),]
    clinicalData_lipo     <- completedClinicalData[rownames(currentLayer_lipo),]
    
    response_controls     <- matrix(rep("Control", nrow(currentLayer_controls)), ncol = 1)
    response_obese        <- matrix(rep("Obese", nrow(currentLayer_obese)), ncol = 1)
    response_lipo         <- matrix(rep("Lipodystrophy", nrow(currentLayer_lipo)), ncol = 1)
    
    response_patients     <- matrix(c(rep("Patient", nrow(currentLayer_obese)), 
                                      rep("Patient", nrow(currentLayer_lipo))), ncol = 1)
    
   
    rownames(response_controls) <- rownames(currentLayer_controls)
    rownames(response_obese)    <- rownames(currentLayer_obese)
    rownames(response_lipo)     <- rownames(currentLayer_lipo)
    rownames(response_patients) <- c(rownames(currentLayer_obese),rownames(currentLayer_lipo))
    
    train_lipo_ctrl         <- rbind(currentLayer_lipo, currentLayer_controls)
    train_obese_ctrl        <- rbind(currentLayer_obese, currentLayer_controls)
    train_patients_ctrl     <- rbind(currentLayer_obese, currentLayer_lipo, currentLayer_controls)

    clinicalData_lipo_ctrl         <- rbind(clinicalData_lipo, clinicalData_controls)
    clinicalData_obese_ctrl        <- rbind(clinicalData_obese, clinicalData_controls)
    clinicalData_patients_ctrl     <- rbind(clinicalData_obese, clinicalData_lipo, clinicalData_controls)
    
        
    response_lipo_ctrl      <- rbind(response_lipo, response_controls)
    response_obese_ctrl     <- rbind(response_obese, response_controls)
    response_patients_ctrl  <- rbind(response_patients, response_controls) 
    
    if(comparison == "patients_controls")
    {
      training_X   <- scale(train_patients_ctrl)
      training_y   <- response_patients_ctrl
      clinicalData <- as.matrix(clinicalData_patients_ctrl[, c("AGE", "SEX")])
      
    }
    
    if(comparison == "bariatric_controls")
    {
      training_X   <- scale(train_obese_ctrl)
      training_y   <- response_obese_ctrl
      clinicalData <- as.matrix(clinicalData_obese_ctrl[, c("AGE", "SEX")])
    }
    
    
    if(comparison == "lipodystrophy_controls")
    {
      training_X   <- scale(train_lipo_ctrl)
      training_y   <- response_lipo_ctrl
      clinicalData <- as.matrix(clinicalData_lipo_ctrl[, c("AGE", "SEX")])
      
    }
    
    # We don't want to shrink the coefficients of the clinical data
    penaltyFactor <- c(rep(1, ncol(training_X)), 0, 0)
    training_X    <- cbind(training_X,clinicalData)
    #####################################################################################
    ######## Let's use glmnet to make selections:
    alpha <- 0.1
    
    # The exact split for cross validation has an impact on the variable selected.
    # Repeat nRepeats times, and report in the manuscript the largest selected set 
    # (since we are most interested in hypothesis generation, and less concerned 
    # about falsely selected variables at this stage)
    nRepeats     <- 1000
    fittedModels <- BiocParallel::bplapply(1:nRepeats, selectVariables_adjustingForAgeAndSex, training_X, training_y, penaltyFactor)
    nSelections  <- unlist(BiocParallel::bplapply(fittedModels, numberOfSelectedVariables))
    
    ### LARGEST SET OF VARIABLES ###
    chosenK      <- which(nSelections == max(nSelections))[1]
    
    fittedGlmnet <- fittedModels[[chosenK]] 
    fittedCoefs  <- coef(fittedGlmnet,  s = "lambda.min")  
    # Store the selected variables
    selectedVariables_alpha <- rownames(fittedCoefs)[which(fittedCoefs!=0)]
    
    # Make predictions using the trained model
    commonIDs                <- intersect(rownames(currentLayer), rownames(completedClinicalData))
    
    clinicalData_commonIDs   <- as.matrix(completedClinicalData[commonIDs,c("AGE", "SEX")])
    currentLayer_commonIDs   <- currentLayer[commonIDs,]
    dataForPrediction        <- cbind(currentLayer_commonIDs,clinicalData_commonIDs)    
    
    predicted                <- predict(fittedGlmnet, dataForPrediction, s= "lambda.min", type = "response")
    
    allFittedCoefs           <- BiocParallel::bplapply(fittedModels, coefficientsOfVariables)
    
  
    write.csv(selectedVariables_alpha, file = paste("results_multivariableAnalysis/", comparison,"/", 
                                                    dataset_name, "selectedVariables_alpha", alpha,"_age_sex_adjusted.csv", sep = ""))
    write.csv(predicted, file = paste("results_multivariableAnalysis/", comparison,"/", 
                                      dataset_name, "predictions_alpha", alpha, "_age_sex_adjusted.csv", sep = ""))
    save(fittedCoefs, file = paste("results_multivariableAnalysis/", comparison,"/", 
                                   dataset_name, "fittedCoefs_alpha", alpha, "_age_sex_adjusted.RData", sep = ""))
    save(allFittedCoefs, file = paste("results_multivariableAnalysis/", comparison,"/", 
                                    dataset_name, "allFittedCoefs_alpha", alpha, "_age_sex_adjusted.RData", sep = ""))
    write.csv(nSelections, file = paste("results_multivariableAnalysis/", comparison,"/",
                                        dataset_name, "nSelections_alpha", alpha,"_age_sex_adjusted.csv", sep = ""))
  } 
}